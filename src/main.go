package main

import (
    "flag"
    "log"
    "io/ioutil"
    "strings"
    "os"

    "./dep/iconv-go"
)

var (
    _file string
    _from string
    _to string

    _LOG *log.Logger
)

func main() {
    flag.StringVar(&_file, "file", "", "file which will be converted")
    flag.StringVar(&_from, "from", "", "from encoding")
    flag.StringVar(&_to, "to", "", "to encoding")
    flag.Parse()

    if _file == "" || _from == "" || _to == "" {
        flag.Usage()
        return
    }

    f, err := os.OpenFile(".log", os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0644)
    if err != nil {
        log.Println(err)
    }
    defer f.Close()

    _LOG = log.New(f, "", log.LstdFlags | log.Lmicroseconds)
    if err := convert(_file, _from, _to); err == nil {
        _LOG.Printf("convert %s successed", _file)
    } else {
        _LOG.Printf("convert %s failed, err = %v", _file, err)
    }
}

func convert(path, from, to string) (err error) {
    content, err := ioutil.ReadFile(path)
    if err != nil {
        _LOG.Println(err)
        return err
    }

    converted, err := iconv.ConvertString(string(content), _from, _to)
    if err != nil {
        _LOG.Println(err)
        return err
    }

    err = ioutil.WriteFile(path, []byte(converted), 0644)
    if err != nil {
        _LOG.Println(err)
        return err
    }
    return nil
}

func test() { // 检查 log 中的哪个文件没有在 diff_files 中出现
    content, err := ioutil.ReadFile("log")
    if err != nil {
        log.Println(err)
        return
    }
    strs_log := strings.Split(string(content), "\n")

    content, err = ioutil.ReadFile("diff_files")
    if err != nil {
        log.Println(err)
        return
    }
    strs_diff := strings.Split(string(content), "\n")

    is_in_strs_diff := func(str string) bool {
        for _, v := range strs_diff {
            index_end := strings.Index(v, ".lua")
            index_begin := strings.LastIndex(v, "/")
            if index_begin == -1 || index_end == -1 {
                log.Printf("can not get lua file name, line = %s", v)
                return false
            }
            lua_file_name := v[index_begin + 1 : index_end + 4]
            if lua_file_name == str {
                return true
            }
        }
        return false
    }

    for _, v := range strs_log {
        index_end := strings.Index(v, ".lua")
        index_begin := strings.LastIndex(v, "/")
        if index_begin == -1 || index_end == -1 {
            log.Printf("can not get lua file name, line = %s", v)
            return
        }
        lua_file_name := v[index_begin + 1 : index_end + 4]
        if !is_in_strs_diff(lua_file_name) {
            log.Printf("%s do not appear ;-)", v)
        }
    }
}
